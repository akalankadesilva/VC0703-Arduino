#include <Stream.h>
#include <Arduino.h>
#define PIC_SMALL 0x22
#define PIC_MEDIUM 0x11
#define READ_PIC_TIMEOUT 10000
#define RESPONSE_TIMEOUT 5000

class vc0703{
public:
  void init(Stream &serial,int camSerial);
  bool waitResponse(byte response[],int length);
  void sendCommand(byte commands[],int length);
  bool reset() ;
  bool takePic();
  long getFrameSize();
  bool setSize(int picSize);
  int readPic(byte* frame,int length);
  bool resume();
  int serialNo=0;
private:
  Stream* CAM_SERIAL;
  int fBufPtr=0;
  byte* sequence;

};



