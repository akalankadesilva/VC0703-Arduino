#include "vc0703.h"


long frameSize=0;
byte frameBuffer[128];
int readLength=0;
int thisFrameLength=0;
vc0703 camera;


void setup() { 
  Serial.begin(115200);
  Serial1.begin(115200);
  pinMode(PC0, OUTPUT);
  camera.init(Serial1,0);
  

}

void loop() {

  Serial.println("#Resetting camera: ");
  
  Serial.println(camera.reset());
  
  Serial.println("#Setting pic size: ");
  Serial.println(camera.setSize(PIC_MEDIUM));
  Serial.println("#Taking pic: ");
  Serial.println(camera.takePic());
  frameSize=camera.getFrameSize();
  Serial.print("#Frame size: ");
  Serial.println(frameSize);
  readLength=0;
  while(readLength<frameSize){
    thisFrameLength=camera.readPic((byte*)frameBuffer,128);
    readLength+=thisFrameLength;
    for(int i=0;i<thisFrameLength;i++){
      if(frameBuffer[i]<0x10)
        Serial.print('0');
      Serial.print(frameBuffer[i],HEX);
    }
    if(thisFrameLength>0)
      Serial.println();
  }
  Serial.println("#End of pic");
  Serial.println(); 
  delay(10000); 
}
