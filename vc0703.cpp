#include "vc0703.h"

void vc0703::init(Stream &serial,int camSerial){
  CAM_SERIAL=&serial;
  serialNo=camSerial;
}
bool vc0703::waitResponse(byte response[],int length){
  long start=millis();
  int respPtr=0;
  char c;
  while(millis()-start<RESPONSE_TIMEOUT){
    if(CAM_SERIAL->available()){
      c=CAM_SERIAL->read();
      
      if(c!=response[respPtr]){
        continue;
      }
      
      respPtr++;
      if(respPtr>=length){
        return true;
      }
    }
  }
  return false;
}
void vc0703::sendCommand(byte commands[],int length){
  for(int i=0;i<length;i++){
    CAM_SERIAL->write(commands[i]);
    
  }
  
}
bool vc0703::reset(){ 
  byte commands[4]={0x56,(byte)serialNo,0x26,0x00};
  byte response[5]={0x76,(byte)serialNo,0x26,0x00,0x00};
  sendCommand(commands,4);
  delay(3000);
  while(CAM_SERIAL->available()){
    CAM_SERIAL->read();
  }
}


bool vc0703::takePic(){
  fBufPtr=0;
  byte commands[5]={0x56,(byte)serialNo,0x36,0x01,0x00};
  byte response[5]={0x76,(byte)serialNo,0x36,0x00,0x00};
  sendCommand(commands,5);
  return waitResponse(response,5);
}
long vc0703::getFrameSize(){
  long frameSize=0;
  long start;
  int readPtr=0;
  byte c;
  byte commands[5]={0x56,(byte)serialNo,0x34,0x01,0x00};
  byte response[5]={0x76,(byte)serialNo,0x34,0x00,0x04};
  sendCommand(commands,5);
  if(!waitResponse(response,5))return false;
  start=millis();
  while(millis()-start<RESPONSE_TIMEOUT&&readPtr<4){
    if(CAM_SERIAL->available()){
      c=CAM_SERIAL->read();
      frameSize=frameSize+(c<<((4-++readPtr)*8));
    }
  }
  return frameSize;
}
bool vc0703::setSize(int picSize){
  byte commands[9]={0x56,(byte)serialNo,0x31,0x05,0x04,0x01,0x00,0x19,(byte)picSize};
  byte response[5]={0x76,(byte)serialNo,0x31,0x00,0x00};
  sendCommand(commands,9);
  return waitResponse(response,5);
}
int vc0703::readPic(byte* frame,int length){
  long start=0;
  long readCount=0;
  byte commands[16]={0x56,(byte)serialNo,0x32,0x0c,0x00,0x0a,0x00,0x00,(byte)(fBufPtr>>8),(byte)(fBufPtr&0xFF),0x00,0x00,(byte)(length>>8),(byte)(length&0xFF),0x00,0x0a};
  byte response[5]={0x76,(byte)serialNo,0x32,0x00,0x00};
  byte endFlag[2]={0xFF,0xD9};
  int endFlagPtr=0;
  char c;
  
  sendCommand(commands,16);
  if(!waitResponse(response,5))return 0;
  start=millis();
  while(millis()-start<READ_PIC_TIMEOUT&&length>readCount&&endFlagPtr<2){
    if(CAM_SERIAL->available()){
      c=CAM_SERIAL->read();
      *frame++=c;
      readCount++;
          
      if(endFlag[endFlagPtr]==c){
        endFlagPtr++;
      }
      else{
        endFlagPtr=0;     
      }
    }
  }

  if(!waitResponse(response,5))return 0;
 

  fBufPtr=fBufPtr+readCount;
 return readCount;
}
bool vc0703::resume(){
  byte commands[5]={0x56,(byte)serialNo,0x36,0x01,0x02};
  byte response[5]={0x76,(byte)serialNo,0x36,0x00,0x00};
  sendCommand(commands,5);
  return waitResponse(response,5);
}





